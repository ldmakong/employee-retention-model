# Employee Retention Model

## Requirements

- Python >=3.0
- [Anaconda](https://www.anaconda.com/)

First create a custom-named environment:

    $ conda env create -f environment.yml -n ENV_NAME

Second, update the environment to ensure all the necessary packages for the project are installed :

    $ conda env update -f environment.yml -n ENV_NAME

Third, activate the environment :

    $ source activate ENV_NAME

Finally, open jupyter to view the project notebooks :

    $ jupyter notebook 


## Goal

In this project, we are given a dataset about employees of a company by its human ressources department. They want us to build a model which will predict whether or not a permanent employee will likely leave. With such predictions, they want to roll out in advance appropriate initiatives to retain employees who might likely leave.

## Approach

After exploring and cleaning the given dataset with key features, we used and compared different classification-based methods (L1-regularized logistic regression, L2-regularized logistic regression, Random Forests, Gradient Boosting) in order to find the best model which will predict an employee status. 


## Results

The best model is based on Random Forests since its area under the ROC curve (AUROC) is the highest with 0.992. 

## Author

MAKONG Ludovic / [@ldmakong](https://gitlab.com/ldmakong)


